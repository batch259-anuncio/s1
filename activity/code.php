<?php

function getFullAddress($country, $city, $province, $specificAddress){
return " $specificAddress, $city, $province, $country";
}

// function getLetterGrade($grade){

//     if ($grade <= 100 && $grade >= 98){
//         return 'A+';
//     } elseif ($grade <= 97 && $grade >= 95){
//         return 'A';
//     } elseif ($grade <= 94 && $grade >= 92) {
//         return 'A-';
//     }elseif ($grade <= 91 && $grade >= 89) {
//         return 'B+';
//     }elseif ($grade <= 88 && $grade >= 86) {
//         return 'B';
//     }elseif ($grade <= 85 && $grade >= 83) {
//         return 'B-';
//     }elseif ($grade <= 82 && $grade >= 80) {
//         return 'C+';
//     }elseif ($grade <= 79 && $grade >= 77) {
//         return 'C';
//     }elseif ($grade <= 76 && $grade >= 75) {
//         return 'C-';
//     }else{
//         return 'F';
//     }
   
// }

function getLetterGrade($grade) {
    return ($grade <= 100 && $grade >= 98) ? "$grade is equivalent to A+" : 
           (($grade <= 97 && $grade >= 95) ? "$grade is equivalent to A" :
           (($grade <= 94 && $grade >= 92) ? "$grade is equivalent to A-" :
           (($grade <= 91 && $grade >= 89) ? "$grade is equivalent to B+" :
           (($grade <= 88 && $grade >= 86) ? "$grade is equivalent to B" :
           (($grade <= 85 && $grade >= 83) ? "$grade is equivalent to B-" :
           (($grade <= 82 && $grade >= 80) ? "$grade is equivalent to C+" :
           (($grade <= 79 && $grade >= 77) ? "$grade is equivalent to C" :
           (($grade <= 76 && $grade >= 75) ? "$grade is equivalent to C-" : "$grade is equivalent to F"))))))));
}