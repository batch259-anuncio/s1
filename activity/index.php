<?php
require_once "./code.php";
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S1</title>
</head>

<body>
    <h1>Full Address</h1>
    <p><?= getFullAddress('Philipphines','Quezon City','Metro Manila', '3F CashwynnBldg ., Timog Avenue'); ?></p>
    <p><?= getFullAddress('Philipphines','Makati City','Metro Manila', '3F Enzo Bldg., Beundia Avenue'); ?></p>

    <h1>Letter-Based Grading</h1>
    <p><?=  getLetterGrade(87); ?></p>
    <p><?=  getLetterGrade(94); ?></p>
    <p><?=  getLetterGrade(74); ?></p>
</body>

</html>